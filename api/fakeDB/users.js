const Repository = require("./repository");
const crypto = require("crypto");

class UsersRepository extends Repository {
  async create(attrs) {
    attrs.id = this.randomId();
    const salt = crypto.randomBytes(8).toString("hex");
    const records = await this.getAll();
    //@@TODO@@
    // doplnit check, ci user podla emailu existuje,
    //ak ano tak vratit chybu a nevytvorit pouzivatela s rovnakym menom
    const record = {
      ...attrs,
      password: crypto
        .pbkdf2Sync(attrs.password, salt, 1000, 64, "sha512")
        .toString("hex"),
    };

    records.push(record);

    await this.writeAll(records);

    return record;
  }
}

module.exports = new UsersRepository("fakeDB/files/users.json");
