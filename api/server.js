const express = require("express");
const { json } = require("body-parser");

const app = express();

const usersRepo = require("./fakeDB/users");

const start = async () => {
  await initDB();
  //   const user = await usersRepo.create({
  //     name: "Slavo",
  //     email: "salvo@uniza.sk",
  //     password: "Heslo1234",
  //   });
  //   console.log(user);
  //   const user = await usersRepo.getOneBy({ id: "e22a88a1" });
  //   if (user) {
  //     usersRepo.update(user.id, {
  //       name: "Peter",
  //     });
  //   }
  app.listen(5000, () => {
    console.info(`Listening at http://localhost:5000`);
  });
};

const initDB = async () => {
  //@@TODO@@
  //Ak neexistuje v DB ziaden user, vytvorte jedneho lubovolneho pouzivatela
};

start();
